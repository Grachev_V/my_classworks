#include <stdio.h>
#include <string.h>
#define N 10
void countChar(char *buf)
{   
	int cnt, i, j;
	i = 0;
	while(buf[i]){	
		cnt = 1; j = i+1;
        if (buf[i] != '*')
			while(buf[j])
				if (buf[i] == buf[j++]){
					cnt++;
					buf[j-1] = '*';
				}
		if (buf[i] != '*')
			printf("%c - %d\n",buf[i],cnt);		
		i++;
	}
}

int main()
{   
	char str[N];
	puts("Enter a string");
	fgets(str, N, stdin);
    str[strlen(str)] = 0;
	countChar(str);
	return 0;
}